package jp.alhinc.hirose_takumi.calculate_sales;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

class Branch {

	String code; //支店コード
	String name; //支店名
	long totalSales; //売上合計

	void setCode(String code) {
		this.code = code;
	}
	void setName(String name) {
		this.name = name;
	}
	void setTotal(long totalSales) {
		this.totalSales = totalSales;
    }
    
    public String toString() {
        return String.format("%s,%s,%s", code, name, totalSales);
    }

}

public class CalculateSales {

	public static void main(String[] args) {

		//マップ作成(支店コード：支店名)
		Map<String, String> map = new LinkedHashMap<>();
		//マップ作成(支店コード：売上合計)
        Map<String, Long> map1 = new LinkedHashMap<>();
        
        try {
            BufferedReader br = null;
            try {
                File file = new File(args[0], "branch.lst");
                if (!file.exists()) {
                    System.out.print("ファイルが存在しません");
                    return;
                }

                br = new BufferedReader(new FileReader(file));

                String line;
                while((line = br.readLine()) != null) {
                    String[] branch = line.split(",");
                    if (branch.length != 2) {
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }
                    if(branch[0].matches("^[0-9]{3}") == false) {
                        System.out.println("支店定義ファイルのフォーマットが不正です");
                        return;
                    }
                    map.put(branch[0], branch[1]);
                    map1.put(branch[0], 0L);
                }
            } finally {
                br.close();
            }

            File dir = new File(args[0]);
            File files[] = dir.listFiles();
            Arrays.sort(files);
            List<Integer> saleList = new ArrayList<>(); //売上ファイル名リスト
            for(int i = 0; i < files.length; i++) {
                String fileName = files[i].getName();
                if (fileName.matches("^[0-9]{8}.rcd$") == true) {
                    int baseName = Integer.parseInt(fileName.substring(0,fileName.lastIndexOf('.')));
                    saleList.add(baseName);
                    for (int j = 1; j < saleList.size(); j++) {
                        if (saleList.get(j) != saleList.get(j - 1) + 1) {
                            System.out.println("売上ファイル名が連番になっていません");
                            return;
                        }
                    }

                    try {
                        File file = new File(args[0], fileName);
                        br = new BufferedReader(new FileReader(file));
                        String code = br.readLine();
                        int sale = Integer.parseInt(br.readLine());
                        if(map.containsKey(code) == false) {
                            System.out.println(fileName + "の支店コードが不正です");
                            return;
                        }
                        Long total = map1.get(code) + sale;
                        if(total.toString().length() > 10) {
                            System.out.println("合計金額が10桁を超えました");
                            return;
                        }
                        map1.put(code, total);

                        if(br.readLine() != null) {
                            System.out.println(fileName + "のフォーマットが不正です");
                            return;
                        }
                    } finally {
                        br.close();
                    }
                }
            }

            List<Branch> branches = new ArrayList<>();

            for (String key : map.keySet()) {
                Branch branch = new Branch();
                branch.setCode(key);
                branch.setName(map.get(key));
                branch.setTotal(map1.get(key));

                branches.add(branch);
            }

            BufferedWriter bw = null;
            try {
                File file = new File(args[0], "branch.out");
                bw = new BufferedWriter(new FileWriter(file));
                for(Branch branch : branches) {
                    bw.write(branch.toString());
                    bw.newLine();
                }
            } finally {
                bw.close();
            }
        } catch (Exception e) {
            System.out.println("予期せぬエラーが発生しました");
            return;
        }

	}
}
